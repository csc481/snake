// This grabs all the code that helps the game work
requirejs(['../engine/Mochi', './grid', './snake', './score', './food', './snakecontrol'],
function(Mochi, Grid, Snake, Score, Food, SnakeControl) {

    /** Transitions from the main game state */
    class EndGameState extends Mochi.GameState {
        constructor(name, score) {
            super(name);
            this.score = score;
        }

        init (game) {
            this.game = game;
            this.game.input.onKeyPress("Enter", this.restart.bind(this));
        }

        restart() {
            this.game.swapState("intro");
        }

        /**
         * Draws the end game screen.
         */
        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.width, this.game.height);
            ctx.fillStyle = "gray";
            ctx.fillRect(0, 0, this.game.width, this.game.height);
            // draw the score, and if they have a high score, show it.
            this.score.draw(ctx);
            if (this.score.save()) {
                this.score.drawHighScore(ctx, this.game.width / 2, this.game.height / 2);
            } else {
                this.score.drawGameOver(ctx, this.game.width / 2, this.game.height / 2);
            }
            ctx.restore();
        }
    }

    /** Used for waiting for additional players / joining additional players */
    class NetworkState extends Mochi.GameState {
        constructor(name, main) {
            super(name);
            this.main = main;
        }

        init(game) {
            this.game = game;
        }

        /** When we enter, we need to connect with a socket */
        enter() {
            // this socket can be used to communicate with the server
            this.main.socket = io();
            this.main.socket.on('client_ready_1', this.clientsReady1.bind(this));
            this.main.socket.on('client_ready_2', this.clientsReady2.bind(this));
            this.main.socket.on('clients_full', this.full.bind(this));
        }

        clientsReady1() {
            // both clients are ready, enter the game
            this.main.player = 1;
            this.game.swapState("main");
        }
        clientsReady2() {
            // both clients are ready, enter the game
            this.main.player = 2;
            this.game.swapState("main");
        }

        full() {
            // it's full, go to full state.
            this.game.swapState("full");
        }

        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.width, this.game.height);
            ctx.fillStyle = "gray";
            ctx.fillRect(0, 0, this.game.width, this.game.height);
            ctx.fillStyle = "#f8f8f8";
            ctx.font = '32px tahoma';
            ctx.textAlign = "center";
            ctx.fillText("Waiting for players...", this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);

            ctx.restore();
        }
    }

    class FullState extends Mochi.GameState {
        constructor(name) {
            super(name);
        }

        init(game) {
            this.game = game;
        }

        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.width, this.game.height);
            ctx.fillStyle = "gray";
            ctx.fillRect(0, 0, this.game.width, this.game.height);
            ctx.fillStyle = "#f8f8f8";
            ctx.font = '32px tahoma';
            ctx.textAlign = "center";
            ctx.fillText("Game is full, cannot join.", this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);

            ctx.restore();
        }
    }

    /**
     * The main class that runs the snake game.
     */
    class SnakeGame {
        constructor() {
            this.game = new Mochi.Game({
                width: window.innerWidth,
                height: window.innerHeight,
                fps: 60,
                startState: "load"
            });

            // Create the factory that generates food and one that creates snakes.
            this.snakeFactory = new Snake.Factory();
            this.foodFactory = new Food.Factory();
            this.gridFactory = new Grid.Factory();
            // assume single player at first.
            this.player = 1;
            // put the HUD on the bottom of the canvas with some tolerance
            this.score = new Score(0, this.game.height - Score.HEIGHT);

            // Add states for the game
            this.game.addState(
                new Mochi.GameStates.LoadState("load", [this.gridFactory, this.snakeFactory, this.foodFactory], "intro")
            );
            // The first state is an intro screen.
            this.game.addState(
                new Mochi.GameStates.IntroState("intro", ["Enter", " "], "playerSelect").setGameName("Icecream Snake")
            );
            // The second state is an player select screen.
            this.game.addState(
                new Mochi.GameState("playerSelect").setup(
                    this.selectUpdate.bind(this), this.selectDraw.bind(this), this.selectInit.bind(this), this.selectEnter.bind(this)
                )
            );
            this.game.addState(
                new NetworkState("server", this)
            );
            this.game.addState(
                new FullState("full")
            );
            // The main game state, uses set up for convenience to set our methods.
            this.game.addState(
                new Mochi.GameState("main").setup(
                    this.update.bind(this), this.draw.bind(this), this.init.bind(this), this.enter.bind(this)
                )
            );
            // lastly, a state for ending the game.
            this.game.addState(
                new EndGameState("end", this.score)
            );
        }

        selectInit() {};

        selectEnter() {
            this.numPlayers = 1;
            this.game.input.onKeyDown("w", this.swapPlayers.bind(this));
            this.game.input.onKeyDown("s", this.swapPlayers.bind(this));
            this.game.input.onKeyDown("Enter", this.transition.bind(this));
        };

        swapPlayers() {
            this.numPlayers = this.numPlayers === 1 ? 2 : 1;
        }

        transition() {
            // transition based on what we selected
            if (this.numPlayers === 1)
                this.game.swapState("main");
            else
                this.game.swapState("server");
        }

        selectUpdate(ms) {
        };

        selectDraw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());

            ctx.fillStyle = "#3a3a3a";
            ctx.fillRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());

            ctx.fillStyle = "#f8f8f8";
            ctx.font = '48px tahoma';
            ctx.textAlign = "center";
            ctx.fillText("Player Select", this.game.getPixelWidth() / 2, this.game.getPixelHeight() * 0.3);
            ctx.font = '32px tahoma';
            let players1 = (this.numPlayers === 1 ? "*" : " ") + "1 Player";
            ctx.fillText(players1, this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);
            let players2 = (this.numPlayers === 2 ? "*" : " ") + "2 Players (server)";
            ctx.fillText(players2, this.game.getPixelWidth() / 2, this.game.getPixelHeight() * 0.7);

            ctx.restore();
        };

        /** Called right before the first time a state is entered. */
        init() {};

        /**
         * Calls whenever this state is being entered.
         * Whenever the state is entered, a completely new snake game is initialized.
         */
        enter () {
            // Load the score that is stored locally (not async)
            this.score.load();
            // Create the grid that objects are bound to
            this.grid = this.gridFactory.make(this.game, SnakeGame.CELLS, this);
            // create the snake and add it to the grid.
            let p1Controls = new SnakeControl(this.game.input, {
                up:'w',
                down:'s',
                left:'a',
                right:'d'
            });
            let p2Controls = new SnakeControl(this.game.input, {
                up:'i',
                down:'k',
                left:'j',
                right:'l'
            });
            this.snake = this.snakeFactory.makeDefaultSnake(this.grid);
            if (this.player === 1)
                this.snake.add(p1Controls);
            else
                this.snake.add(p2Controls);
            this.grid.addEntity(this.snake);

            if (this.numPlayers === 2) {
                this.snake2 = this.snakeFactory.make(this.grid, 3, 10, 3);
                if (this.player === 2)
                    this.snake2.add(p1Controls);
                else
                    this.snake2.add(p2Controls);
                this.grid.addEntity(this.snake2);
            }
            // Reset all controls on the game's input.
            this.game.input.resetBindings();
            // Add bindings for input to change the snake.

            this.snake.bind(this.game.input);
            if (this.numPlayers === 2) {
                this.snake2.bind(this.game.input);
                if (this.player === 1)
                    this.bindNetworkedSnakes(this.snake, this.snake2);
                else
                    this.bindNetworkedSnakes(this.snake2, this.snake);
            }
            // tell the food system to spawn its first piece of food.
            this.generateRandomFood();
        }

        bindNetworkedSnakes(snake1, snake2) {
            // when we send a direction, tell the server.
            snake1.events.subscribe("direction", function(direction) {
                console.log("Sending my snake's direction.");
                this.socket.emit("snake_direction", direction);
            }.bind(this));
            // when we hear about a direction, modify snake 2.
            this.socket.on("snake_direction", function(direction) {
                console.log("Receiving their snake's direction.");
                snake2.goDirection(direction);
            });
            this.socket.on("snake_end", function() {
                console.log("Our other snake lost...");
                this.end();
            }.bind(this));
        }

        /**
         * Renders the snake game
         * @param ctx The context needed.
         */
        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.width, this.game.height);
            this.grid.draw(ctx);
            this.score.draw(ctx);
            ctx.restore();
        }

        /** The update loop */
        update(ms) {
            this.grid.update(ms);
            this.food.update(ms);
        }

        /**
         * Generates a random food.
         */
        generateRandomFood() {
            this.food = this.foodFactory.make(this.grid, 0, 0);
            // get new random position.
            this.food.reset(this.grid, this.grid.size);
            this.grid.addEntity(this.food);
        }

        /** Begins the entire game */
        start() {
            this.game.start();
        }

        end() {
            if (this.numPlayers === 2) {
                this.socket.emit("snake_end");
                this.socket.disconnect();
            }
            this.game.swapState("end");
        }
    }
    SnakeGame.CELLS = 16;

    var snake = new SnakeGame();
    // Initialize and begin the game.
    snake.start();
    console.log(snake);
});

