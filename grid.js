/**
 * Created by Kenny on 9/18/2017.
 */
define(['../engine/Mochi', './snake'], function(Mochi, Snake) {

    class GridFactory {
        constructor() {
            // Create assets for all the different images required.
            this.tileAsset = new Mochi.Content.Asset("assets/grid_tile.png");
        }

        load() {
            return this.tileAsset.load();
        }

        make(game, cells, main) {
            return new Grid(game.width, game.height, cells, main, this.tileAsset);
        }
    }

    /**
     * The grid class manages a list of entities and how their coordinates translate into pixels.
     * Works as a scene management object for the entire game.
     */
    class Grid {
        constructor(maxWidth, maxHeight, size, main, tileAsset) {
            this.x = 0;
            this.y = 0;
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;
            this.main = main;
            /** The number of tiles make up the rows/columns of the grid */
            this.size = size;
            /** The number of pixels per tile */
            this.pixelsPerUnit = Grid._CalculateDimensions(maxWidth, maxHeight, size);
            /** The grid tile style */
            this.tile = new Mochi.Graphics.Sprite(tileAsset, { height: 1, width: 1 });
            /** The entities bound to this grid */
            this.entities = [];
            this.collisionManager = new Mochi.Physics.CollisionManager(0, 0, size, size, 1, 1);
            // grid is the unit system for this object
            this.drawManager = new Mochi.Graphics.UnitDrawManager(this);
            console.log(this);
        }

        addEntity (entity) {
            this.entities.push(entity);
            this.collisionManager.add(entity);
        }

        removeEntity (entity) {
            this.entities.splice(this.entities.indexOf(entity), 1);
            this.collisionManager.remove(entity);
        }

        endGame() {
            this.main.end();
        }

        eatFood() {
            this.main.score.value += 10;
        }

        /** Converts pixels to grid units */
        pixelsToUnits(px) {
            return Math.floor(px / this.pixelsPerUnit);
        }
        /** Converts game units to pixels for drawing */
        unitsToPixels(units) {
            return units * this.pixelsPerUnit;
        }

        /** Draws all the entities, passes in the grid for conversions */
        draw(ctx) {
            // Draws the grid structure
            this._drawGrid(ctx);
            // Draws all the objects bound to this grid
            this.drawManager.draw(ctx, this.entities);
        }
        /** Used to draw the grid-like structure */
        _drawGrid(ctx) {
            for (var i = 0; i < this.size; i++) {
                for (var j = 0; j < this.size; j++) {
                    let x = this.unitsToPixels(i);
                    let y = this.unitsToPixels(j);
                    this.tile.draw(ctx, x, y, this.unitsToPixels(1), this.unitsToPixels(1), 0);
                }
            }
        }
        /** Updates all of the entities in the grid */
        update(ms) {
            for (let i = 0; i < this.entities.length; i++) {
                if (this.entities[i] instanceof Snake.Snake) {
                    this.entities[i].update(ms);
                }
            }
            this.collisionManager.update(ms);
            this.collisionManager.collide();
        }

        /** Internal function that calculates the pixels per tile */
        static _CalculateDimensions(width, height, cells) {
            var square = (width >= height ? height : width);
            return Math.floor(square / cells);
        }
    }

    return {
        Grid: Grid,
        Factory: GridFactory
    };
});